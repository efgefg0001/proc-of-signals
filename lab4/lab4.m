% ???????
% ?????? ??. ???. 40

function lab4
    t = [-5:0.01:+5];
    n = length(t);
    dt = t(2) - t(1);
    f = [-(n - 1)/2:+(n - 1)/2] / (n * dt);
    
    u0 = 1 * exp(-(t/1) .^ 2);
    
    ua = u0 + uniferr(t);
    ub = u0 + normerr(t);

    subplot(3, 2, 1);
    plot(t, u0,...
         t, ua,...
         t, despectrize(spectrize(ua) .* fbutti(f, 2, dt)));
    title('infinite butterworth uniform');
    
    subplot(3, 2, 2);
    plot(t, u0,...
         t, ub,...
         t, despectrize(spectrize(ub) .* fbutti(f, 2, dt)));
    title('infinite butterworth normal');

    subplot(3, 2, 3);
    plot(t, u0,...
         t, ua,...
         t, despectrize(spectrize(ua) .* fbuttf(f, 2, 1)));
    title('finite butterworth uniform');
    
    subplot(3, 2, 4);
    plot(t, u0,...
         t, ub,...
         t, despectrize(spectrize(ub) .* fbuttf(f, 2, 1)));
    title('finite butterworth normal');
    
    subplot(3, 2, 5);
    plot(t, u0,...
         t, ua,...
         t, despectrize(spectrize(ua) .* fgausf(f, 2)));
    title('finite gaussian uniform');

    subplot(3, 2, 6);
    plot(t, u0,...
         t, ub,...
         t, despectrize(spectrize(ub) .* fgausf(f, 2)));
    title('finite gaussian normal');
    
    
function s = spectrize(x)
    s = fftshift(fft(x));
    
function x = despectrize(s)
    x = ifft(ifftshift(s));


function e = uniferr(t)
    n = length(t);
    e = zeros(1, n);
    c = round(unifrnd(5, 6));
    p = round(unifrnd(1, length(t), 1, c));
    e(p) = unifrnd(-1, +1, 1, c) * 0.7;
    
function e = normerr(t)
    e = normrnd(0, 0.1, 1, length(t));
    
    
function h = fbutti(f, b, dt)
    h = sqrt(1 ./ (1 + (sin(pi * f * dt) / sin(pi * b * dt)) .^ 4));
    
function h = fbuttf(f, d, n)
    h = 1 ./ (1 + (f / d) .^ (2 * n));

function h = fgausf(f, s)
    h = exp(-(f/s) .^ 2);

    

    

    



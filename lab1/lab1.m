% ???????
% ?????? ??. ???. 9, 10

function lab1
    clear;
    x0 = 2;
    A = 1;
    T = 1.5;
    sigma = 0.5;
    step = 0.1;
    restorestep = 0.01;
    buildPlots(A, x0, T, sigma, step, restorestep);
end

function buildPlots(A, x0, T, sigma, step, restorestep)
    val_restore = -x0:restorestep:x0;
    x = val_restore;
    orig_rect = impulse(x, T);
    orig_gauss = gauss(A, x, sigma);
    x = -x0:step:x0;
    sample_rect = impulse(x, T);
    sample_gauss = gauss(A, x, sigma);
            
    rest_rect = restore(val_restore, sample_rect, step, x);
    rest_gauss = restore(val_restore, sample_gauss, step, x);
            
    shg;

    figure(1);
    plot(val_restore, orig_rect, 'r', val_restore, rest_rect, 'b');
    legend('original', 'restored');
    title('rectangular');
            
    figure(2);
    plot(val_restore, orig_gauss, 'r', val_restore, rest_gauss, 'b');
    legend('original', 'restored');
    title('gauss');            
end

function v = impulse(x, T)
    n = length(x);
    v = zeros(1, n);
    for k=1:n
        if abs(x(k)) <= T
            v(k) = 1;
        end
    end
end
 
function v = gauss(A, x, sigma)
    v = (A * exp(-x .^ 2 / sigma));
end
        
function v = restore(val_restore, y, step, x)
    n = length(val_restore);
    v = zeros(1, n);
    for k=1:length(y)
        v = v + y(k) .* sinc((val_restore - x(k)) / step);
    end
end


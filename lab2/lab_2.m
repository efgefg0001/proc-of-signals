%example : 
% lab_2(10,10,1)
function lab_2
sigma = 3;
f = 3;
a = 1;
rectangle_signal = GetRectangleSignal(f);
diapason_array = GetDiapason(f);
gauss_signal = GetGaussSignal(sigma,a,f);

Start = cputime; 
DPF_rectangle_signal = DPF(rectangle_signal);
Elapsed = cputime - Start;
fprintf('time dft rect = %.6f\n', Elapsed);

Start = cputime; 
BPF_rectangle_signal = BPF(rectangle_signal);
Elapsed = cputime - Start;
fprintf('time fft rect = %.6f\n', Elapsed);

Start = cputime; 
DPF_gauss_signal = DPF(gauss_signal);
Elapsed = cputime - Start;
fprintf('time dft gauss = %.6f\n', Elapsed);

Start = cputime; 
BPF_gauss_signal = BPF(gauss_signal);
Elapsed = cputime - Start;
fprintf('time fft gauss = %.6f\n', Elapsed);


figure
subplot(1, 2, 1);
plot(diapason_array,DPF_rectangle_signal,'k'), grid, title('dft rect');

subplot(1, 2, 2);
plot(diapason_array,BPF_rectangle_signal,'k'), grid, title('fft rect');

figure
subplot(1, 2, 1);
plot(diapason_array,DPF_gauss_signal,'k'), grid, title('dft gauss');
subplot(1, 2, 2);
plot(diapason_array,BPF_gauss_signal,'k'), grid, title('fft gauss');

%fprintf('size1 = %.6f\n', size(diapason_array));
%fprintf('size2 = %.6f\n', size(rectangle_signal));
%fprintf('a = %.6f\n', diapason_array);
%fprintf('size = %.6f\n', CalculateDelta(f));

%figure
%plot(diapason_array,gauss_signal,'b'), grid
%hold on;
%plot(diapason_array,restore_gauss_signal,'r'), grid

end

function delta = CalculateDelta(f)
    delta = 1.0/(2*f);
end

function array = GetDiapason(f)
deltaX = CalculateDelta(f);
startX = -2.0*f;
size = (2.*f)./deltaX;
signal = zeros(2*size+1,1);

for i = 1:2*size+1
    signal(i) = startX+(i-1).*deltaX;
end;
array = signal;    
end

function array = GetRectangleSignal(f)
deltaX = CalculateDelta(f);
size = (2*f)/deltaX;
signal = zeros(2*size+1,1);

startIndex = size/2+1;
finishIndex = 1.5*size+1;

for i = startIndex:finishIndex
    signal(i) = 1;
end;
array = signal;    
end

function array = GetGaussSignal(sigma, a, f)
sigma2 = sigma*sigma;
deltaX = CalculateDelta(f);
size = (2*f)/deltaX;
signal = zeros(2*size+1,1);

X = -2.0*f;

for i = 1:2*size+1
    signal(i) = a*exp(-X*X/sigma2);
    X = X + deltaX;
end;
array = signal;    
end

function array = DPF(array)
k = size(array);
k = k(1);
signal = zeros(k,1);

for i = 1:k
    sum_re = 0;
    sum_im = 0;
    for j = 1:k
        temp = -2.0 * pi * (i-1) * (j-1) / k;
        sum_re = sum_re + array(j)*cos(temp)*(-1)^j; 
        sum_im = sum_im + array(j)*sin(temp)*(-1)^j;
    end;
    
    signal(i) = abs(sum_re * sum_re - sum_im * sum_im);
end;

array = signal;
end

function array = BPF(array)
k = size(array);
k = k(1);
signal = zeros(k,1);

k2 = k/2 - 1;

for i = 1:k
    sum_re = 0;
    sum_im = 0;
    additionalExp = -2.0 * pi * (i-1) / k;
    for j = 1:k2
        mainExp = -2.0 * pi * (i-1) * (j-1) / k2;
        
        s0_re = -array(2*j-1)*cos(mainExp); 
        s0_im = -array(2*j-1)*sin(mainExp); 
        
        s1_re = array(2*j)*cos(mainExp + additionalExp); 
        s1_im = array(2*j)*sin(mainExp + additionalExp); 
        
        sum_re = sum_re + s0_re + s1_re; 
        sum_im = sum_im + s0_im + s1_im;
    end;
    
    signal(i) = abs(sum_re * sum_re - sum_im * sum_im);
end;

array = signal;
end



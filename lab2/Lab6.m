%Lab6(10,1,10)
function Lab6(sigma_signal, a_signal, f)
diapason_array = GetDiapason(f);
signal_m = 0;
gauss_signal = GetGaussSignal(sigma_signal,signal_m,a_signal,f);

impulse_koff = a_signal/2;
impulse_count = 10;
impulse_koff_gauss = a_signal/2;
gauss_disturbance = GetGaussDisturbance(impulse_koff_gauss,GetSize(f));
impulse_disturbance = GetImpulseDisturbance(GetSize(f),impulse_koff,impulse_count);

signal_with_gauss_disturbance = AddDisturbance(gauss_signal,gauss_disturbance);
signal_with_impulse_disturbance = AddDisturbance(gauss_signal,impulse_disturbance);

viner_filter_gauss_disturbance = VinerFilter(signal_with_gauss_disturbance);
viner_filter_impulse_disturbance = VinerFilter(signal_with_impulse_disturbance);

figure
plot(diapason_array,signal_with_gauss_disturbance,'b'), grid;
hold on;
plot(diapason_array,viner_filter_gauss_disturbance,'r'), grid, title('������ ������ ��� ������������ ������� � ����������� �������');

figure
plot(diapason_array,signal_with_impulse_disturbance,'b'), grid;
hold on;
plot(diapason_array,viner_filter_impulse_disturbance,'r'), grid, title('������ ������ ��� ������������ ������� � ���������� �������');

end

function delta = CalculateDelta(f)
    delta = 1.0/(2*f);
end

function size = GetSize(f)
deltaX = CalculateDelta(f);
size = (4*f)/deltaX+1;
end

function array = GetDiapason(f)
deltaX = CalculateDelta(f);
startX = -2.0*f;
size = (2.*f)./deltaX;
signal = zeros(2*size+1,1);

for i = 1:2*size+1
    signal(i) = startX+(i-1).*deltaX;
end;
array = signal;    
end

function array = GetGaussSignal(sigma, m, a, f)
sigma2 = sigma*sigma;
deltaX = CalculateDelta(f);
size = (2*f)/deltaX;
signal = zeros(2*size+1,1);

X = -2.0*f;

for i = 1:2*size+1
    signal(i) = a*exp(-((X-m)^2)/sigma2);
    X = X + deltaX;
end;
array = signal;    
end

function array = GetImpulseDisturbance(size, koff, kol)

signal = rand(kol,2);

for i=1:kol
    signal(i,1) = signal(i,1)*koff;
    signal(i,2) = floor(signal(i,2)*size);
end

array = signal;
end

function array = GetGaussDisturbance(koff, kol)

signal = unifrnd(5,6,kol,2);

for i=1:kol
    if (mod(i,10) == 0)
        signal(i,1) = rem(signal(i,1)*koff,koff);
    else
        signal(i,1) = 0;
    end
    signal(i,2) = i;
end

array = signal;
end

function array = AddDisturbance(Signal, Disturbance)

DisturbanceCount = size(Disturbance);
DisturbanceCount = DisturbanceCount(1);

SignalWithDisturbance = Signal;

for i=1:DisturbanceCount
    Index = Disturbance(i,2);
    SignalWithDisturbance(Index) = SignalWithDisturbance(Index) + Disturbance(i,1);
end

array = SignalWithDisturbance;
end

function array = GetDisturbanceFromSignal(Signal)

count = size(Signal);
count = count(1);
spectre = zeros(count,1);

SideCount = 5;
Eps = 0.1;

for i=SideCount+1:count-SideCount
    
    sideValue = 0;
    for j=-SideCount:SideCount
       sideValue = sideValue + Signal(i+j);        
    end
    sideValue = sideValue/(2*SideCount+1);
    spectre(i) = sideValue;
    
    if (abs(Signal(i)-sideValue) > Eps*Signal(i))
        spectre(i) = Signal(i)-sideValue;
    else
        spectre(i) = 0;
    end
   
end

array = spectre;


end

function array = GetSignalPower(Signal)
FFT_Signal = fft(Signal);
array = FFT_Signal.*conj(FFT_Signal);
end

function array = VinerFilter(SignalWithDisturbance)
Disturbance = GetDisturbanceFromSignal(SignalWithDisturbance);
PureSignal = SignalWithDisturbance - Disturbance;

NFFT = size(SignalWithDisturbance);
NFFT = NFFT(1);

PureSignalPower = GetSignalPower(PureSignal);
DisturbancePower = GetSignalPower(Disturbance);

H = zeros(NFFT,1);
for i=1:NFFT
    H(i)=PureSignalPower(i)/(PureSignalPower(i)+DisturbancePower(i)); %������������ �������
end

SignalSpectre = fft(SignalWithDisturbance);
array = ifft(SignalSpectre.*H);
end
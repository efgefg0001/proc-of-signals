% ???????
% ?????? ??. ???. 31

function lab3
    hw = 5;
    dt = 0.05;
    
    t = [-hw:dt:+hw];
    
    r = abs(t) <= 1;
    g = 1 * exp(-(t/1) .^ 2);
    
    subplot(3, 1, 1);
    plot(t, r, t, r, t, myconv(r, r) * dt);
    legend('rect', 'rect', 'conv');
    title('rect rect');
    
    subplot(3, 1, 2);
    plot(t, r, t, g, t, myconv(r, g) * dt);
    legend('rect', 'gauss', 'conv');    
    title('rect gauss');
    
    subplot(3, 1, 3);
    plot(t, g, t, g, t, myconv(g, g) * dt);
    legend('gauss', 'gauss', 'conv');    
    title('gauss gauss');
    
function z = myconv(x, y)
    z = ifft(fft(x) .* fft(y));
    k = floor(length(z) / 2);
    z = [z(k+1:end), z(1:k)];

    